var mongoose = require('mongoose');
var DbConnection = require('../database/dbconnection');
DbConnection.Get();

var ChatSchema = new mongoose.Schema({
    username : String,
    message : String,
    group_id: Number,
    createdOn: { type: Date, 'default': Date.now }
});

module.exports = mongoose.model('Chat', ChatSchema);