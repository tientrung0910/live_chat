var mongoose = require('mongoose');
var DbConnection = require('../database/dbconnection');
DbConnection.Get();

var UserSchema = new mongoose.Schema({
    name : String,
    username : String,
    password : String,
    token: String,
    createdOn: { type: Date, 'default': Date.now }
});

module.exports = mongoose.model('User', UserSchema);