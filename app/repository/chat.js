const { ObjectId } = require('mongodb');
var ChatModel = require('../model/chatModel');

class ChatRepository {
    async get() {
        return await ChatModel.find({group_id: 1});
    }

    async set(username, message, group_id) {
        if(username && message){
            var chat = new ChatModel({
                username,
                message,
                group_id
            })
            chat.save();
            return chat;
        }
    }

    async delete(group_id, id) {
        return await ChatModel.deleteOne({group_id: group_id, _id: id});
    }
}

module.exports = new ChatRepository()