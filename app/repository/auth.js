const jwt = require('jsonwebtoken')
var UserModel = require('../model/userModel');

class AuthRepository {
    async login(username, password) {
        var check = await UserModel.find({ username, password });
        if (check[0]) {
            var token = await this.genToken(check[0]._id.toString());
            await this.setToken(username, token);
            return token;
        } else {
            return false;
        }
    }

    async genToken(user_id) {
        return jwt.sign({ id: user_id }, process.env.JWT_KEY);
    }

    async setToken(username, token){
            var update = await UserModel.updateOne({username: username}, {token: token});
    }

    async auth(id, token){
        var check = await UserModel.findOne({ _id: id,token, token });
        return check ?? false;
}
}


module.exports = new AuthRepository()