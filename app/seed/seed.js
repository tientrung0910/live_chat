require('dotenv').config();
var UserModel = require('../model/userModel');

async function seed(){
    try {
        await UserModel.deleteMany();
        var num_records = 5;
        for (var i = 0; i < num_records; i++) {
            let msg = new UserModel({
                name: 'user' + i,
                username: 'username' + i,
                password: 123456,
                token: ''
            })
            await msg.save()
                .then(doc => {
                })
                .catch(err => {
                    console.error(err)
                });
        }
        process.exit();
    } catch (error) {
        console.log(error);
    }
}


seed();

