- Required:
    - docker setup

- Install steps:
    - docker-compose up -d 
    - npm install 
    - npm run seed      

- Run project 

    - npm start 
    - npm test       

- achievement
    - User authentication with basic username/password login. The credentials can be hardcoded. 
    - Creation of a single chat room upon server startup. No need to create multiple rooms. 
    - Persistent storage of chat messages in a Database. 
    - Sending and receiving messages in the chat room. The client must be able to fetch the room messages 
    - RESTful endpoints for message sending, and message retrieval. 
    - Unit testing 
    - Web socket 
    - Deletion of messages by clients (API) 


- web endpoint: 
    - home page: http://localhost:3001

- api endpoint.
    - Get all messages 
        - GET http://localhost:3001/message 
    - Delete message by id 
        - DELETE http://localhost:3001/message/:id 
    - Login 
        - POST http://localhost:3001/auth 

- web socket endpoint: 
    - http://localhost:3001



- attachment 
    - postman API: /posman/live_chat_api.postman_collection.json 
    - socket: posman does not support export sockets, so it needs to be added manually 
        - new -> websocket request (BETA) -> change type from 'raw' to 'Socket.IO' -> url: http://localhost:3001

- User acount (required run cmd "npm run seed")
    -  username1 / 123456
    -  username2 / 123456
    -  username3 / 123456
    -  username4 / 123456
    -  username5 / 123456
        