const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require('path');
const cookieParser = require('cookie-parser');
var cors = require('cors')
require('dotenv').config();
const chatRepository = require('./app/repository/chat');
const APP_PORT = process.env.APP_PORT;
app.use(express.urlencoded({
  extended: true
}))
app.use(express.static(__dirname + '/public'));
app.use(cors({ origin: APP_PORT, withCredentials: true }));
app.use(cookieParser());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// socket
app.get('/socket.io/socket.io.js', (req, res) => {
  res.sendFile(__dirname + '/node_modules/socket.io/client-dist/socket.io.js');
});

require("./routes/api")(app);
require("./routes/web")(app);

io.on('connection', (socket) => {
  console.log('A user connected');

  socket.on('disconnect', () => {
    console.log('A user disconnected');
  });

  socket.on('message', (data) => {
    console.log('Received message:', data);
    io.emit('message', data);
    chatRepository.set(data.username, data.message, 1);
  });
});


http.listen(APP_PORT, () => {
  console.log("Server started on port " + APP_PORT);
});

module.exports = app;