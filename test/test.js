const mongoose = require("mongoose");
const request = require("supertest");
const app = require("../app");
const authRespository = require('../app/repository/auth');

async function getToken(){
    var token = await authRespository.login("username1", "123456");
    return "Bearer " + token;
}

// login
describe("POST /auth", () => {
    describe("login successfull", () => {
      test("should respond with a 200 status code", async () => {
        const response = await request(app).post("/auth").send({
            username: "username1",
            password: "123456"
        })
        expect(response.statusCode).toBe(200)
      })
    })
  })

  describe("POST /auth", () => {
    describe("missing username or password", () => {
      test("should respond with a 400 status code", async () => {
        const response = await request(app).post("/auth").send({
            username: "username1"
        })
        expect(response.statusCode).toBe(400)
      })
    })
  })

  describe("POST /auth", () => {
    describe("wrong username or password", () => {
      test("should respond with a 400 status code", async () => {
        const response = await request(app).post("/auth").send({
            username: "username1",
            password: "1234567"
        })
        expect(response.statusCode).toBe(401)
      })
    })
  })

  describe("GET /message", () => {
    describe("missing Authorization token", () => {
      test("should respond with a 401 status code", async () => {
        const response = await request(app).get("/message").send()
        expect(response.statusCode).toBe(401)
      })
    })
  })

  describe("GET /message", () => {
    describe("successfull", () => {
      test("should respond with a 200 status code", async () => {
        const response = await request(app).get("/message").set('Authorization', await getToken()).send()
        expect(response.statusCode).toBe(200)
      })
    })
  })

  describe("GET /message", () => {
    describe("successfull", () => {
      test("should respond data of object", async () => {
        const response = await request(app).get("/message").set('Authorization', await getToken()).send()
        expect(typeof response.body.data).toBe("object");

      })
    })
  })