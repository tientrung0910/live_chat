const jwt = require('jsonwebtoken')
const authRespository = require('../app/repository/auth');

const auth = async(req, res, next) => {
    try {
        var token = req.header('Authorization');
        token = token.replaceAll('Bearer ', '');
        const data = jwt.verify(token, process.env.JWT_KEY)
        const user = await authRespository.auth(data.id,token);
        if (!user) {
            res.status(200).send({ message: 'Not authorized to access this resource' })
        }
        req.user = user
        req.token = token
        next()
    } catch (error) {
        res.status(401).send({ message: 'Not authorized to access this resource' })
    }

}
module.exports = auth