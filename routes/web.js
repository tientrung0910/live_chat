var express = require('express');
const app = express();
const auth = require('../middleware/auth')
const authRespository = require('../app/repository/auth');
const chatRepository = require('../app/repository/chat');
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()

module.exports = function(app){
  app.get('/', async function (request, response) {
    const username = request.cookies.username;
    const token = request.cookies.token;
    if (token) {
      const messages = await chatRepository.get(1);
      response.render('chat', { title: 'Demo Live chat', username: username, messages: messages });
    } else {
      response.render('login', {});
    }
  });
  
  app.get('/login', async function (request, response) {
    response.render('login', {});
  });
}
