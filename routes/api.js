var express = require('express');
const app = express();
const auth = require('../middleware/auth')
const authRespository = require('../app/repository/auth');
const chatRepository = require('../app/repository/chat');
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()

module.exports = function(app){
// get all meesage
app.get('/message', auth, async (req, res) => {
  try {
    const message = await chatRepository.get(1);
    res.json({ data: message });
  } catch (error) {
    res.status(500).send('Something broke!')
  }
});

// delete message
app.delete('/message/:id', auth, async (req, res) => {
  try {
    var id = req.params.id;
    await chatRepository.delete(1, id);
    res.send("deleted!");
  } catch (error) {
    res.status(500).send('Something broke!')
  }
});

// login
app.post('/auth', jsonParser, async function (request, response, next) {
  try {
    let username = request.body?.username ?? "";
    let password = request.body?.password ?? "";
    if (username && password) {
      var token = await authRespository.login(username, password);
      if (token) {
        response.send({ username: username, token: token });
      } else {
        response.status(401).send('Unauthorized');
      }
      next();
      response.end();
    }else{
      response.status(400).send('Please enter Username and Password!');
    }
  } catch (error) {
    res.status(500).send('Something broke!')
  }
});

}
